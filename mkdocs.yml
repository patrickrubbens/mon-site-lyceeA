site_name: "Lycée ANGELLIER - Mathématiques - M.Rubbens"
site_url : "https://patrickrubbens.forge.aeif.fr/mon-site-lyceeA/"
copyright: 
    Patrick Rubbens 2023-2024 
    <a rel="license" href="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br/>
    Hébergé sur la forge gérée par l'<a href="https://aeif.fr">AEIF</a>


# nav:

theme:
  name: material
  favicon: xtra/images/penrose.svg
  logo: xtra/images/penrose.svg
  font: false
  language: fr
  features:
    - navigation.instant
    - navigation.tabs
  #  - navigation.expand
    - navigation.top
    - toc.integrate
    - header.autohide
    
  custom_dir: my_theme_customizations/
  palette:
    # Light mode
    - media: "(prefers-color-scheme: light)"
      scheme: default
      primary: indigo
      accent: indigo
      # primary: orange
      # accent: orange
      toggle:
        icon: material/toggle-switch-off-outline
        name: Mode sombre

    # Dark mode
    - media: "(prefers-color-scheme: dark)"
      scheme: slate
      primary: blue
      accent: blue
      # primary: orange
      # accent: orange
      toggle:
        icon: material/toggle-switch
        name: Mode clair

plugins:
  # -search
  - awesome-pages :
      collapse_single_pages: false
      strict: false

markdown_extensions:
    - meta
    - abbr
    - def_list                      # Les listes de définition.
    - attr_list                     # Un peu de CSS et des attributs HTML.
    - footnotes                     # Notes[^1] de bas de page.  [^1]: ma note.
    - admonition                    # Blocs colorés  !!! info "ma remarque"
    - pymdownx.details              #   qui peuvent se plier/déplier.
    - pymdownx.caret                # Passage ^^souligné^^ ou en ^exposant^.
    - pymdownx.mark                 # Passage ==surligné==.
    - pymdownx.tilde                # Passage ~~barré~~ ou en ~indice~.
    - pymdownx.highlight:           # Coloration syntaxique du code
        auto_title: true
        auto_title_map:
            "Python": "🐍 Script Python"
            "Python Console Session": "🐍 Console Python"
            "Text Only": "📋 Texte"
            "E-mail": "📥 Entrée"
            "Text Output": "📤 Sortie"
    - pymdownx.inlinehilite         # pour  `#!python  <python en ligne>`
    - pymdownx.snippets             # Inclusion de fichiers externe.
    - pymdownx.tasklist:            # Cases à cocher  - [ ]  et - [x]
        custom_checkbox:    false   #   avec cases d'origine
        clickable_checkbox: true    #   et cliquables.
    - pymdownx.tabbed:              # Volets glissants.  === "Mon volet"
        alternate_style: true 
    - pymdownx.keys:                # Touches du clavier.  ++ctrl+d++
        separator: "\uff0b"
    - pymdownx.emoji:               # Émojis  :boom:
        emoji_index:     !!python/name:materialx.emoji.twemoji
        emoji_generator: !!python/name:materialx.emoji.to_svg
    - pymdownx.superfences:
        custom_fences:
          - name: mermaid
            class: mermaid
            format: !!python/name:pymdownx.superfences.fence_code_format
    - pymdownx.arithmatex:
        generic: true
    - toc:
        permalink: ⚓︎
        toc_depth: 2
 

extra_javascript:
  - xtra/javascripts/mathjax-config.js                    # MathJax
  - https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js

extra_css:
  - xtra/stylesheets/qcm.css ##
  - xtra/stylesheets/ajustements.css  # ajustements

