
# Progression Terminale ST2S

Lycée Angellier - 2023/2024

**Automatismes :**

* Faire une séance de 10 questions par semaine soit 20 minutes + 10 minutes de correction (ou deux séances de cinq questions : durée 15 minutes).

**Algorithme et programmation :** _Voir avec les collègues de l’année dernière_

* En 1STL : Imports de modules, Fonctions et instructions conditionnelles, boucles type « for » et « While », quelques exemples de listes, utilisation du concours Algoréa avec programmation en Python jusqu’à niveau orange.

## Chapitre 1 : Séries statistiques à deux variables.

* Série statistique à deux variables.
* Nuage de points.
* Ajustement affine (avec notamment méthode des moindres carrés avec la calculatrice).

## Chapitre 2 : Fonction inverse.

* Rappel sur fonction polynômes degré 2 et 3, calculer et utiliser la dérivée.
* Rappel cours sur fonction dérivée et sens de variation.
* Fonction inverse : définition, courbe, dérivée et propriétés
* Nouveau : fonction de la forme

## Chapitre 3 : Suites arithmétiques.

* Rappel : Calculer des termes d’une suite numérique, utiliser une suite arithmétique et
géométrique.
* Moyenne arithmétique-croissance linéaire.
* Expression du terme général d’une suite arithmétique (attention, pas au programme de la
première).
* Somme des premiers termes d’une suite arithmétique.

## Chapitre 4 : Probabilités conditionnelles.

* Rappel : tableau croisé d’effectifs, construire et utiliser un arbre pondéré pour des épreuves
indépendantes.
* Probabilité Conditionnelle.
* Formule des probabilités totales.
* Indépendance de deux événements.

## Chapitre 5 : Fonctions exponentielles

* Rappel : Calculer avec des exposants (faire avant avec automatismes).
* Définition avec représentation graphique.
* Sens de variation.
* Propriétés algébriques.
* Application : taux moyen (voir livre page 51)

## Chapitre 6 : Suites géométriques.

* Moyenne géométrique- croissance exponentielle.
* Expression du terme général d’une suite géométrique (attention, pas au programme de la
première).
* Somme des premiers termes d’une suite géométrique.

## Chapitre 7 : Variables aléatoires discrètes.

* Rappel : variable aléatoire.
* Rappel : Epreuve de Bernoulli.
* Loi Binomiale.
* Espérance mathématique.

## Chapitre 8 : Fonction logarithme décimal.

* Une nouvelle fonction.
* Définition et représentation graphique.
* Sens de variation, propriétés algébriques.
* Applications de la fonction Log( voir livre page 51 : ordre de grandeur…. ; repère
logarithmique


_Attention, concernant les TSTL. Une heure en plus pour faire deux fois plus de Chapitre apitres et pas les plus simples ( A intégrer à la progression commune)._

**Programme de spécialité STL :**

## Chapitre  9 : Compléments sur les fonctions (mais pas au Bac).

* Dérivation et composée de fonction.
* Primitives et composées de fonctions.

## Chapitre  10 : Intégration.

* Intégrale d’une fonction positive.
* Intégrale d’une fonction.
* Application de l’intégrale.

## Chapitre 11 : La fonction exponentielle.

* Définition et propriétés.
* Limites, dérivées et primitives

## Chapitre  12 : Fonction logarithme

* Logarithme népérien d’un réel.
* Limites, dérivées et primitives.

## Chapitre  13 : Equations différentielles.

* Notation d’équation différentielle.
* Equation différentielle .
* Equation différentielle .
