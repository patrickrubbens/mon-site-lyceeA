# Progression Seconde 
-----------------------------------------

Lycée Angellier - 2023/2024

**3 fils rouges :**

* γ : calcul littéral
* α : algorithmique
* δ : démonstrations

Rituel : Course aux nombres en début de séance (quand on a le temps)

## Chapitre 1. Fonctions et lectures graphiques:

 > encedrement et notation intervalles : segment de la droite des réels.

 > Variations sans formaliser
 
 > Résolutions graphiques équations et inéquations 

γ : calculs simples ; α : variables et affectation 

## Chapitre 2. proportions - pourcentages

## Chapitre 3. Ensemble de nombres.

  > N : multiples, diviseurs, parité, nombres premiers, pgcd

  α : crible d'Erathosthène ; δ : carré d’un impair

  > D : un peu d'histoire. Pourquoi la base 10 ? 

  α : la base 2

  > Q : Qu'est qu'une fraction? nombres commensurables. 

  α : simplification de fraction, convention $a^{0}=1$ et notation $10^{−n}$

  Un peu d'histoire autour de $\pi$

  > R : Manipuler les nombres réels. 

  δ : $\sqrt{a}$ + $\sqrt{b}$ > $\sqrt{a+b}$ 

  δ : irrationalité de $\sqrt{2}$ 

## Chapitre 4. Repères

 > Milieu

 > Distance

## Chapitre 5. Vecteurs (1) 

  > Parallélogramme

  > Somme géomètrique de 2 vecteurs

  > Relation de Chasles ;

## Chapitre 6. Évolutions 

## Chapitre 7. Fonctions 

  > Variations et extremums (def formelles)

  > fonctions affines

  > fonction carré

  > fonction inverse

γ : problèmes du 1er degré (equations-inéquations) ; α : fonctions

## Chapitre 8. Vecteurs (2)
  
  > Coordonnées
  > Problèmes de géométrie plane. 
  > Colinéarité, Déterminant.

δ et α: vecteurs colinéaires ssi det = 0

## Chapitre 9. Simulations et échantillonnage. 

  > Probabilités

  > réunion et intersection d'évènements - diagramme

  > Tableaux et arbres

  > loi de probabilité

## Chapitre 10. Équations de droites
  
  > Caractérisation vectorielle

  > Equations cartésiennes

  > Systèmes

δ et α : en utilisant le determinant
 

## Chapitre 12. Fonctions (2):

  > Fonction racine carrée

  > Fonction cube 

δ : variations

δ : positions relatives des courbes, variations ;

## Chapitre 13. Retour sur des problèmes de géométrie plane

  > Projection orthogonale


## Chapitre 14. Statistiques

  > Moyenne et écart type
